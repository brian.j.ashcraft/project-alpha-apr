from django.forms import ModelForm
from tasks.models import Task


class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
        ]


# class Task(models.Model):
#     name = models.CharField(max_length=200)
#     start_date = models.DateTimeField()
#     due_date = models.DateTimeField()
#     is_completed = models.BooleanField(default=False)
#     project = models.ForeignKey(
#         Project,
#         related_name="tasks",
#         on_delete=models.CASCADE,
#     )
#     assignee = models.ForeignKey(
#         User,
#         related_name="tasks",
#         on_delete=models.CASCADE,
#         null=True,
#     )
